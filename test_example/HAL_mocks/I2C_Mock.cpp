#include "I2C_Mock.h"
#include <functional>
#include <assert.h>

static std::function<bool()> _i2cInit;
static std::function<bool()> _i2cDeinit;
static std::function<bool()> _i2cIsInitialized;
static std::function<bool(uint8_t address, uint8_t *pDst, uint16_t dataSize)> _i2cRead;
static std::function<bool(uint8_t address, const uint8_t *pSrc, uint16_t dataSize)> _i2cWrite;

I2C_Mock::I2C_Mock()
{
    assert(!_i2cInit || !_i2cDeinit || !_i2cIsInitialized || !_i2cRead || !_i2cWrite);
    _i2cInit = [this]() { return i2cInit(); };
    _i2cDeinit = [this]() { return i2cDeinit(); };
    _i2cIsInitialized = [this]() { return i2cIsInitialized(); };
    _i2cRead = [this](uint8_t address, uint8_t *pDst, uint16_t dataSize) { return i2cRead(address, pDst, dataSize); };
    _i2cWrite = [this](uint8_t address, const uint8_t *pSrc, uint16_t dataSize) { return i2cWrite(address, pSrc, dataSize); };
}

I2C_Mock::~I2C_Mock()
{
    _i2cInit = {};
    _i2cDeinit = {};
    _i2cIsInitialized = {};
    _i2cRead = {};
    _i2cWrite = {};
}

bool i2cInit() { return _i2cInit(); }

bool i2cDeinit() { return _i2cDeinit(); }


bool i2cIsInitialized() { return _i2cIsInitialized(); }


bool i2cRead(uint8_t address, uint8_t *pDst, uint16_t dataSize)
{
    return _i2cRead(address, pDst, dataSize);
}

bool i2cWrite(uint8_t address, const uint8_t *pSrc, uint16_t dataSize)
{
    return _i2cWrite(address, pSrc, dataSize);
}