#pragma once
class bar
{
private:
    /* data */
public:
    bar() = default;
    virtual ~bar() = default;
    void doBarStuff();
};
