#pragma once
#include <stdint.h>
#include <stdbool.h>

namespace gauge {

/**
 * @brief LTC2943 alert listener
 */
class LTC2943Listener
{
  public:
    virtual ~LTC2943Listener() = default;
    virtual void on_temperature_alert() = 0;
    virtual void on_voltage_alert() = 0;
};

}// namespace gauge
