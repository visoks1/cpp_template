#pragma once
#include "LTC2943Listener.h"
namespace gauge {

/**
 * @brief LTC2943 - Multicell Battery Gas Gauge with Temperature, Voltage and Current Measurement
 * Your task is to create and implement very simple chip control module API. Using your API other programmers should be able to:
 * Change chip's ADC mode into one of these modes: automatic mode, scan mode, manual mode, sleep.
 * Get chip's ADC mode.
 * Check if temperature alert is pending.
 */
class LTC2943 final
{
  public:
    /**
     * @brief error code indicating that no error occurred
     * true == NoError
     * false == Error
     * would be nice to have enum and specify what kind of error but for now.... meh
     */
    using IsSuccess = bool;

    /**
     * @brief ADC modes
     * @todo why is it here? 
     */
    enum class Adc_mode : uint8_t {
        AUTOMATIC = 0xC0,
        SCAN = 0x80,
        MANUAL = 0x40,
        SLEEP = 0x00
    };
    explicit LTC2943(LTC2943Listener &listener);
    /**
     * @brief initialize the i2c module
     * 
     * @return true if initialization is successful
     */
    static IsSuccess init();

    /**
     * @brief deinitialize the i2c module
     * @note 
     * @return true if deinitialization is successful
     */
    static IsSuccess deinit();

    /**
     * @brief Get the ADC mode object
     * 
     * @param mode read value
     * @return true if ADC mode is read successfully
     */
    static IsSuccess get_adc_mode(Adc_mode &mode);

    /**
     * @brief Set the ADC mode object
     * 
     * @param mode the new ADC mode
     * @return true if ADC mode is set successful
     */
    static IsSuccess set_adc_mode(const Adc_mode mode);

    /**
     * @brief Check if any alert is pending
     * 
     * @return false if no error occurred during check routine
     */
    IsSuccess check_alerts();

  private:
    static bool write_single(const uint8_t reg, const uint8_t data);
    static bool read_single(const uint8_t reg, uint8_t &data);

    LTC2943Listener &m_listener;
};

}// namespace gauge
