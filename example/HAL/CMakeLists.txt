add_library(HAL STATIC i2c.c)
target_include_directories(HAL PUBLIC ${CMAKE_CURRENT_LIST_DIR})
target_link_libraries(
    HAL
    PRIVATE project_options
        #   be evil - turn off compiler warnings 
        #   project_warnings 
    )
target_compile_definitions(HAL PUBLIC HAL)

